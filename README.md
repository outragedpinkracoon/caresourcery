# CareSourcery

Welcome to CareSourcery, a care home for retired wizards, sorcerers and adventurers. 

This app will let you manange the rooms and residents of the care home.

![Screenshot](screenshots/1.png?raw=true "Screenshot")

## Running Locally

Navigate to where your `.puma-dev` folder lives (usually the root of your user directory) and map to the port 3003

```
cd .puma-dev
echo 3003 > caresourcery.dev
```

Install gems using bundler

```
bundle install
```

Create the dev / test database, run migrations and seed data.
```
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails db:seed
```

Start the server
```
bundle exec rails s

```
Navigate to https://caresourcery.dev to view the site. 

The site will still work on localhost:3003 if you don't want to use puma or have issues with your local ssl cert.

## Logging In

The seed data sets up a user with the email 'me@example.com' with the password 'password'.

## Using a different port

You can set the port in `.env.development` and update your .puma-dev 'caresourcery' file to match.
