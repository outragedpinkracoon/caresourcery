FactoryBot.define do
  factory :room do
    sequence :door_number
  end
end
