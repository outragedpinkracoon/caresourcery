FactoryBot.define do
  factory :resident do
    first_name 'Drizzt'
    last_name "Do'urden"

    trait :with_room do
      association :room, factory: :room, strategy: :build
    end
  end
end
