require 'rails_helper'
require_relative './shared/authentication.rb'

RSpec.describe ResidentsController, type: :controller do
  let(:user) { FactoryBot.create :user }

  describe 'when the user is authenticated' do
    before do
      sign_in user
    end

    describe 'GET index' do
      before do
        FactoryBot.create :resident, :with_room
        get :index
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the index template' do
        expect(response).to render_template(:index)
      end

      it 'assigns the list of residents' do
        expect(assigns(:residents).count).to eq(1)
      end
    end

    describe 'GET show' do
      before do
        resident = FactoryBot.create :resident, :with_room, first_name: 'Belwar'
        get :show, params: { id: resident.id }
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the show template' do
        expect(response).to render_template(:show)
      end

      it 'assigns the correct resident' do
        expect(assigns(:resident).first_name).to eq('Belwar')
      end
    end

    describe 'GET new' do
      before do
        post :new
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the new template' do
        expect(response).to render_template(:new)
      end
    end

    describe 'GET edit' do
      before do
        FactoryBot.create :resident, :with_room, id: 1
        post :edit, params: { id: 1 }
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the edit template' do
        expect(response).to render_template(:edit)
      end

      it 'assigns the correct resident' do
        expect(assigns(:resident).id).to eq(1)
      end
    end

    describe 'POST create' do
      context 'when the create is successful' do
        before do
          FactoryBot.create :room, id: 1
          params = {
            resident: {
              first_name: 'Belwar',
              last_name: 'Dissengulp',
              room_id: 1
            }
          }
          post :create, params: params
        end

        it 'has a 302 status code' do
          expect(response.status).to eq(302)
        end

        it 'redirects to the correct resident show page' do
          id = Resident.last.id
          expect(response).to redirect_to("/residents/#{id}")
        end

        it 'creates the resident in the database' do
          expect(Resident.last.first_name).to eq('Belwar')
        end

        it 'associates the resident to the room in the database' do
          resident = Resident.last
          expect(resident.room.id).to eq(1)
        end
      end

      context 'when the create is unsuccessful' do
        before do
          params = {
            resident: {
              first_name: 'Belwar',
              last_name: 'Dissengulp',
              room_id: 1
            }
          }
          post :create, params: params
        end

        it 'has a 200 status code' do
          expect(response.status).to eq(200)
        end

        it 'renders the new template' do
          expect(response).to render_template(:new)
        end

        it 'does not create another room in the database' do
          expect(Resident.count).to be(0)
        end
      end
    end

    describe 'PUT update' do
      context 'when the update is successful' do
        before do
          # room that resident is currently in
          room = Room.create id: 1, door_number: 101
          # room that resident will move to
          Room.create id: 2, door_number: 102

          Resident.create!(
            id: 11,
            first_name: 'Belwar',
            last_name: 'Dissengulp',
            room: room
          )

          params = {
            id: 11,
            resident: {
              first_name: 'Mary',
              last_name: 'Poppins',
              room_id: 2
            }
          }
          put :update, params: params
        end

        it 'has a 302 status code' do
          expect(response.status).to eq(302)
        end

        it 'redirects to the correct resident show page' do
          expect(response).to redirect_to('/residents/11')
        end

        it 'updates the resident in the database' do
          resident = Resident.find(11)
          expect(resident.first_name).to eq('Mary')
          expect(resident.last_name).to eq('Poppins')
          expect(resident.room.id).to eq(2)
        end
      end

      context 'when the update is unsuccessful' do
        before do
          room = Room.create id: 1, door_number: 101

          Resident.create!(
            id: 11,
            first_name: 'Belwar',
            last_name: 'Dissengulp',
            room: room
          )

          params = {
            id: 11,
            resident: {
              first_name: 'Mary',
              last_name: 'Poppins',
              room_id: 2
            }
          }
          put :update, params: params
        end

        it 'has a 200 status code' do
          expect(response.status).to eq(200)
        end

        it 'renders the edit template' do
          expect(response).to render_template(:edit)
        end

        it 'does not update the resident in the database' do
          expect(Resident.find(11).room.id).to be(1)
        end
      end
    end

    describe 'DELETE destroy' do
      before do
        FactoryBot.create :resident, :with_room, id: 1
        delete :destroy, params: { id: 1 }
      end

      it 'has a 302 status code' do
        expect(response.status).to eq(302)
      end

      it 'redirects to the residents index page' do
        expect(response).to redirect_to(residents_path)
      end

      it 'deletes the resident from the database' do
        expect(Resident.count).to be(0)
      end
    end
  end

  describe 'when the user is not authenticated' do
    before do
      sign_out user
    end

    describe 'GET index' do
      before { get :index }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'GET show' do
      before { get :show, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'GET new' do
      before { get :new }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'GET edit' do
      before { get :edit, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'POST create' do
      before { post :create }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'DELETE destroy' do
      before { delete :destroy, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'PUT update' do
      before { put :edit, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end
  end
end
