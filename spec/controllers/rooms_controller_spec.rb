require 'rails_helper'
require_relative './shared/authentication.rb'

RSpec.describe RoomsController, type: :controller do
  let(:user) { FactoryBot.create :user }

  describe 'when the user is authenticated' do
    before do
      sign_in user
    end

    describe 'GET index' do
      before do
        FactoryBot.create :room
        get :index
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the index template' do
        expect(response).to render_template(:index)
      end

      it 'assigns the list of rooms' do
        expect(assigns(:rooms).count).to eq(1)
      end
    end

    describe 'GET show' do
      before do
        room = FactoryBot.create :room, door_number: 12
        get :show, params: { id: room.id }
      end
      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the show template' do
        expect(response).to render_template(:show)
      end

      it 'assigns the correct room' do
        expect(assigns(:room).door_number).to eq(12)
      end
    end

    describe 'GET new' do
      before do
        post :new
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the new template' do
        expect(response).to render_template(:new)
      end
    end

    describe 'GET edit' do
      before do
        FactoryBot.create :room, id: 1
        post :edit, params: { id: 1 }
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the edit template' do
        expect(response).to render_template(:edit)
      end

      it 'assigns the correct room' do
        expect(assigns(:room).id).to eq(1)
      end
    end

    describe 'POST create' do
      context 'when the create is successful' do
        before do
          post :create, params: { room: { door_number: 12 } }
        end

        it 'has a 302 status code' do
          expect(response.status).to eq(302)
        end

        it 'redirects to the correct room show page' do
          id = Room.last.id
          expect(response).to redirect_to("/rooms/#{id}")
        end

        it 'creates the room in the database' do
          expect(Room.last.door_number).to eq(12)
        end
      end

      context 'when the create is unsuccessful' do
        before do
          Room.create! door_number: 12
          post :create, params: { room: { door_number: 12 } }
        end

        it 'has a 200 status code' do
          expect(response.status).to eq(200)
        end

        it 'renders the new template' do
          expect(response).to render_template(:new)
        end

        it 'does not create another room in the database' do
          expect(Room.count).to be(1)
        end
      end
    end

    describe 'PUT update' do
      context 'when the update is successful' do
        before do
          Room.create! id: 1, door_number: 2
          put :update, params: { id: 1, room: { door_number: 123 } }
        end

        it 'has a 302 status code' do
          expect(response.status).to eq(302)
        end

        it 'redirects to the correct room show page' do
          expect(response).to redirect_to('/rooms/1')
        end

        it 'updates the room in the database' do
          expect(Room.find(1).door_number).to eq(123)
        end
      end

      context 'when the update is unsuccessful' do
        before do
          Room.create! id: 1, door_number: 2
          # room with unique door number we will try to also use
          Room.create! door_number: 123
          put :update, params: { id: 1, room: { door_number: 123 } }
        end

        it 'has a 200 status code' do
          expect(response.status).to eq(200)
        end

        it 'renders the edit template' do
          expect(response).to render_template(:edit)
        end

        it 'does not update the room in the database' do
          expect(Room.find(1).door_number).to be(2)
        end
      end
    end

    describe 'DELETE destroy' do
      context 'when the room has no residents' do
        before do
          Room.create! id: 1, door_number: 123
          delete :destroy, params: { id: 1 }
        end

        it 'has a 302 status code' do
          expect(response.status).to eq(302)
        end

        it 'redirects to the rooms index page' do
          expect(response).to redirect_to(rooms_path)
        end

        it 'deletes the room from the database' do
          expect(Room.count).to be(0)
        end
      end

      context 'when the room has a resident attached' do
        before do
          room = Room.create! id: 1, door_number: 123
          FactoryBot.create :resident, room: room

          delete :destroy, params: { id: 1 }
        end

        it 'has a 302 status code' do
          expect(response.status).to eq(302)
        end

        it 'redirects to the rooms index page' do
          expect(response).to redirect_to(rooms_path)
        end

        it 'does not delete the room from the database' do
          expect(Room.count).to be(1)
        end
      end
    end
  end

  describe 'when the user is not authenticated' do
    before do
      sign_out user
    end

    describe 'GET index' do
      before { get :index }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'GET show' do
      before { get :show, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'GET new' do
      before { get :new }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'GET edit' do
      before { get :edit, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'POST create' do
      before { post :create }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'DELETE destroy' do
      before { delete :destroy, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end

    describe 'PUT update' do
      before { put :edit, params: { id: 99 } }
      it_behaves_like 'an unauthenticated action'
    end
  end
end
