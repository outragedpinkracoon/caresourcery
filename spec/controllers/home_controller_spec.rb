require 'rails_helper'
require_relative './shared/authentication.rb'

RSpec.describe HomeController, type: :controller do
  let(:user) { FactoryBot.create :user }

  describe 'when the user is authenticated' do
    before do
      sign_in user
    end

    describe 'GET index' do
      before do
        room = FactoryBot.create :room
        4.times do
          FactoryBot.create :room
        end

        3.times do
          FactoryBot.create :resident, room: room
        end

        get :index
      end

      it 'has a 200 status code' do
        expect(response.status).to eq(200)
      end

      it 'renders the index template' do
        expect(response).to render_template(:index)
      end

      it 'assigns the total rooms' do
        expect(assigns(:total_rooms)).to eq(5)
      end

      it 'assigns the total residents' do
        expect(assigns(:total_residents)).to eq(3)
      end

      it 'assigns the total free rooms' do
        expect(assigns(:available_room_count)).to eq(4)
      end
    end
  end

  describe 'when the user is not authenticated' do
    before do
      sign_out user
    end

    describe 'GET index' do
      before { get :index }
      it_behaves_like 'an unauthenticated action'
    end
  end
end
