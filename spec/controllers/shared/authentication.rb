shared_examples 'an unauthenticated action' do
  it 'has a 302 status code' do
    expect(response.status).to eq(302)
  end

  it 'redirects to the login page' do
    expect(response).to redirect_to(new_user_session_path)
  end
end
