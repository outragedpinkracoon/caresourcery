require_relative '../rails_helper'

RSpec.describe 'Room' do
  describe '#door number' do
    context 'when present and not taken' do
      it 'saves the room' do
        Room.create! door_number: 123
        room = Room.find_by_door_number(123)

        expect(Room.count).to eq(1)
        expect(room.door_number).to eq(123)
      end
    end

    context 'when the door number is already taken' do
      it 'raises an error' do
        Room.create! door_number: 123
        expect { Room.create! door_number: 123 }.to raise_error(
          'Validation failed: Door number has already been taken'
        )
      end
    end

    context 'when door number is not present' do
      it 'raises an error' do
        expect { Room.create! }.to raise_error(
          "Validation failed: Door number can't be blank"
        )
      end
    end
  end

  describe '#residents' do
    context 'when present' do
      it 'raises an error when deleting the room' do
        FactoryBot.create :resident, :with_room
        expect { Room.last.delete }.to raise_error(
          ActiveRecord::InvalidForeignKey
        )
      end
    end

    context 'when none present' do
      it 'can delete the room' do
        FactoryBot.create :room
        Room.last.delete
        expect(Room.count).to eq(0)
      end
    end
  end

  describe '.available' do
    it 'returns only available rooms' do
      room = FactoryBot.create :room, id: 100
      FactoryBot.create :room, id: 101
      FactoryBot.create :room, id: 102

      FactoryBot.create :resident, room: room

      available = Room.available
      expect(available.count).to eq(2)
      expect(available.first.id).to eq(101)
      expect(available.second.id).to eq(102)
    end
  end
end
