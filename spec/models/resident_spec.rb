require_relative '../rails_helper'

RSpec.describe 'Resident' do
  describe 'when all fields are present and correct' do
    let!(:room) { FactoryBot.create :room, id: 1 }
    let(:perform) do
      Resident.create! first_name: 'Belwar', last_name: 'Dissengulp', room: room
    end

    it 'saves the resident to the database' do
      perform
      expect(Resident.count).to eq(1)

      expect(Resident.last.first_name).to eq('Belwar')
      expect(Resident.last.last_name).to eq('Dissengulp')
      expect(Resident.last.room.id).to eq(1)
    end
  end

  describe '#first_name' do
    context 'when not present' do
      let!(:room) { FactoryBot.create :room }
      let(:perform) do
        Resident.create! last_name: 'Dissengulp', room: room
      end

      it 'raises an error' do
        expect { perform }.to raise_error(
          "Validation failed: First name can't be blank"
        )
      end
    end
  end

  describe '#last_name' do
    context 'when not present' do
      let!(:room) { FactoryBot.create :room }
      let(:perform) do
        Resident.create! first_name: 'Belwar', room: room
      end

      it 'raises an error' do
        expect { perform }.to raise_error(
          "Validation failed: Last name can't be blank"
        )
      end
    end
  end

  describe '#room' do
    context 'when not present' do
      let(:perform) do
        Resident.create! first_name: 'Belwar', last_name: 'Dissengulp'
      end

      it 'raises an error' do
        expect { perform }.to raise_error(
          'Validation failed: Room must exist'
        )
      end
    end
  end

  describe '#full_name' do
    it 'returns the first and last names together' do
      resident = FactoryBot.build(
        :resident,
        :with_room,
        first_name: 'Belwar',
        last_name: 'Dissengulp'
      )
      expect(resident.full_name).to eq('Belwar Dissengulp')
    end
  end
end
