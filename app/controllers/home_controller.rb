class HomeController < ApplicationController
  def index
    @total_residents = Resident.count
    @total_rooms = Room.count
    @available_room_count = Room.available.count
  end
end
