class ResidentsController < ApplicationController
  before_action :set_resident, only: %i[show edit update destroy]

  def index
    @residents = Resident.all
  end

  def show; end

  def new
    @resident = Resident.new
  end

  def edit; end

  def create
    @resident = Resident.new(resident_params)

    if @resident.save
      redirect_to @resident, notice: 'Resident was successfully created.'
    else
      render :new
    end
  end

  def update
    if @resident.update(resident_params)
      redirect_to @resident, notice: 'Resident was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @resident.destroy
    redirect_to residents_url, notice: 'Resident was successfully deleted.'
  end

  private

  def set_resident
    @resident = Resident.find(params[:id])
  end

  def resident_params
    params.require(:resident).permit(:first_name, :last_name, :room_id)
  end
end
