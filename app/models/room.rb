class Room < ApplicationRecord
  validates :door_number, presence: true, uniqueness: true
  has_many :residents

  scope :available, lambda {
    includes(:residents).where(residents: { room_id: nil })
  }
end
