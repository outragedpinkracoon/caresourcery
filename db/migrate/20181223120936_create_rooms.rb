class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.integer :door_number, null: false

      t.timestamps
    end
    add_index :rooms, :door_number, unique: true
  end
end
