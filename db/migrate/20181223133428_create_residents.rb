class CreateResidents < ActiveRecord::Migration[5.1]
  def change
    create_table :residents do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.belongs_to :room, foreign_key: true

      t.timestamps
    end
  end
end
