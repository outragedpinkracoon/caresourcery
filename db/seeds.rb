User.delete_all
Resident.delete_all
Room.delete_all

@user = User.new(
  email: 'me@example.com',
  password: 'password',
  password_confirmation: 'password'
)
@user.save!

room101 = Room.create! door_number: 101
room102 = Room.create! door_number: 102
room103 = Room.create! door_number: 103
room104 = Room.create! door_number: 104
Room.create! door_number: 201
Room.create! door_number: 202
Room.create! door_number: 203

Resident.create! first_name: 'Drizzt', last_name: "Do'Urden", room: room101
Resident.create! first_name: 'Artemis', last_name: 'Entreri', room: room102
Resident.create! first_name: 'Wulfgar', last_name: 'Heafstaag', room: room103
Resident.create! first_name: 'Catti', last_name: 'Brie', room: room103
Resident.create! first_name: 'Bruenor', last_name: 'Battlehammer', room: room104
