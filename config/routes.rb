Rails.application.routes.draw do
  resources :residents
  resources :rooms
  devise_for :users
  root to: 'home#index'
end
